﻿
#include <iostream>

int sum = 0;

int** Create(size_t n)
{
    int** M = new int* [n];
    for (size_t i = 0; i < n; ++i)
    {
        M[i] = new int[n];
    }
    return M;
}

void F(int** M, size_t n)
{
    for (size_t i = 0; i < n; ++i)
    {
        delete[] M[i];
    }
    delete[] M;
}

void V(int** M, size_t n)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            M[i][j] = i + j;
        }
    }
}

void Print(int** M, size_t n)
{
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < n; ++j)
        {
            std::cout << M[i][j] << ' ';
        }
        std::cout << std::endl;
    }
}

void Score(int** M, size_t n)
{
    int day = 26;

    for (int j = 0; j < n; ++j)
    {
        for (int i = 0; i < n; ++i)
        {
            if (day % n == i)
            {
                sum = sum + M[i][j];
            }
        }
    }
}

int main() 
{
   

    

    size_t n;

    std::cout << "Size of matrix: ";
    std::cin >> n;

    int** A = Create(n);
    V(A, n);

    Print(A, n);

    F(A, n);

    return 0;
}